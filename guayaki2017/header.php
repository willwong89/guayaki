<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >
	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>

		<div class="hamburger-menu">
	 		 <div class="bar"></div>	
			</div>
		<nav id="site-navigation" class="main-navigation top-bar" role="navigation">
			<div id="menu"  role="navigation">
				<video class="bg-video" width="100%" height="100%" autoplay loop muted id="nav-video">
			  	 <source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/video/blue-clouds.mp4" type="video/mp4">
			  	 	"apologies, video unsupported by this browser"
				</video>
				<?php wp_nav_menu( array('menu' => 'primary', 'menu_class' => 'primary_menu'	)); ?>
			</div>
	 
			<div class="top-bar-middle">
			  <ul class="subnav ctl-nav">
			  	<li>Come To Life</li>
			  	<li><a href="/films">Films</a></li>
			  	<li><a href="http://cometolife.com" target="_blank">Music</a></li>
			  </ul>
			  <a href="/" class="wreath-home-link" id="home-link"><?php get_template_part('/assets/images/svg/guay-wreath.svg'); ?></a>
			  <ul class="subnav mate-nav">
			  	<li>Yerba Mate</li>
			  	<li><a href="/shop">Products</a></li>
			  	<li><a href="/yerba-mate">Learn more</a></li>
			  </ul>
			</div>

			<div class="top-bar-right">
	 	
			</div>
 
		</nav>
 

	<section class="container">
		<?php do_action( 'foundationpress_after_header' );
