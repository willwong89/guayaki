<?php
/*
Single Artist Page
*/
get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <header id="page-header" role="banner">
        <?php $banner = get_field('banner_image'); ?>
        <img class="slide-img" data-player-id="0" src="<?php echo $banner['sizes']['banner'] ?>"  />
        <div class="content">
          <h1><?php the_title(); ?></h1>
        </div>
    </header> 

    <section class="featured-track">  
      <div class="row align-center">
        <div class="columns small-12 large-6">
          <?php if(have_rows('tracks' )) : ?> 
        
            <?php while( have_rows('tracks')) : the_row();   ?>
              <?php if(get_sub_field('featured') == true) : ?>
                  <h5>Featured Track</h5>
                      <?php include( locate_template( 'template-parts/single-player.php' ) ); ?>
                      <?php break; ?>
                <?php endif; ?>
            <?php endwhile; ?> 
            <?php reset_rows(); ?>
          <?php endif; ?>
        </div>
      </div>
    </section>

    <section class="content-section artist-films">
      <h2>Videos</h2>
      <div class="music-film-grid row align-center" >

        <?php if(have_rows('videos' )) : ?> 
          <?php while( have_rows('videos')) : the_row();   ?>
            <?php $image = get_sub_field('thumbnail'); ?>
            <a class="single-film-item swipebox-video" rel="vimeo" href="<?php the_sub_field('video_url'); ?>">
            <div class="grid-bg" style="background-image:url(<?php echo $image['sizes']['medium']?> )"></div>
              <h2><?php the_sub_field('title'); ?></h2>
            </a>
        <?php endwhile; endif; ?>
      </div>  
    </section>


    <section class="content-section artist-bio -slim">
      <div class="row align-center">
        <div class="columns large-7 medium-9 small-12">
           <h2>About</h2>
           <?php the_content(); ?>
      </div>
    </section>

    <section class="content-section artist-tracks">
      <h2>Tracks</h2>
      <div class="row align-center">          
          <?php if(have_rows('tracks' )) : ?> 
            <?php while( have_rows('tracks')) : the_row();   ?>
              <div class="single-track columns small-12 large-7">
                <?php include( locate_template( 'template-parts/single-player.php' ) ); ?>
              </div>
            <?php endwhile; ?> 
          <?php endif; ?>
        </div>
      </div>
    </section>

    <section class="content-section social-links -slim">
      <div class="row">
        <div class="columns large-12 align-center">
          <?php if(get_field('artist_site')) : ?>
            <a href="<?php the_field('artist_site'); ?>"><?php get_template_part('/assets/images/svg/artist-site.svg'); ?></a>
          <?php endif; ?>

          <?php if(get_field('soundcloud_link')) : ?>
            <a href="<?php the_field('soundcloud_link'); ?>"><?php get_template_part('/assets/images/svg/soundcloud.svg'); ?></a>
          <?php endif; ?>

          <?php if(get_field('youtube_link')) : ?>
            <a href="<?php the_field('youtube_link'); ?>"><?php get_template_part('/assets/images/svg/youtube.svg'); ?></a>
          <?php endif; ?>


          <?php if(get_field('twitter_link')) : ?>
            <a href="<?php the_field('twitter_link'); ?>"><?php get_template_part('/assets/images/svg/twitter.svg'); ?></a>
          <?php endif; ?>
          <?php if(get_field('instagram_link')) : ?>
           <a href="<?php the_field('instagram_link'); ?>"><?php get_template_part('/assets/images/svg/instagram.svg'); ?></a>
          <?php endif; ?>

          <?php if(get_field('facebook_link')) : ?>
            <a href="<?php the_field('facebook_link'); ?>"><?php get_template_part('/assets/images/svg/facebook.svg'); ?></a>
          <?php endif; ?>
 
        </div>
      </div>
    </section>

    <section class="content-section other-artists -slim">
    <h2>Musicians</h2>
      <?php $currentID = get_the_ID(); ?>
      <?php $the_query = new WP_Query( array( 'posts_per_page' => 30, 'post_type' => 'artist', 'post__not_in' => array($currentID)) ); ?>
      <?php if ( $the_query->have_posts() ) : ?>
        <div class="artist-slider">
          <?php while ( $the_query->have_posts() ) : $the_query->the_post();  ?>
            <a href="<?php the_permalink(); ?>" class="single-artist"><?php the_post_thumbnail('artist'); ?><span><?php the_title(); ?></span></a>
          <?php endwhile; ?>
        </div>
      <?php endif; ?>
      <div class="row align-center">
        <a href="/" class="big-button">See All</a>
      </div>
      <? wp_reset_query(); ?>
    </section>



  <?php endwhile; ?>
<?php endif; ?>
<?php get_footer();
