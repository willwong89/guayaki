angular.module('mapsApp', [])

.controller('MapCtrl', function ($scope, $http, $document, $location, $anchorScroll) {
        $scope.lastMarker = null;
        navigator.geolocation.getCurrentPosition(function (position) {
            $scope.lat = Number(position.coords.latitude)
            $scope.lng = Number(position.coords.longitude)
            init()
        }, function (err) {
            $scope.lat = 38.4027235
            $scope.lng = -122.8205958
            init()
        }, {
            timeout: 10000
        });
        $scope.lat = 38.4027235;
        $scope.lng = -122.8205958;
        var lat = $scope.lat;
        var lng = $scope.lng;
        var storeDir = "/wp-content/themes/guayaki2017/store-locator/";
        init();

        function init() {
            lat = $scope.lat;
            lng = $scope.lng;
            $http.get("https://www.googleapis.com/fusiontables/v2/query?sql=" +
                    "SELECT * FROM 1HSVe2QAlM1RAZhNXmi0hxLZpIPI-r5du-i4EagPw WHERE Flag < 1 ORDER BY ST_DISTANCE(Location, LATLNG(" + $scope.lat + "," + $scope.lng + ")) LIMIT 20" + "&key=AIzaSyCsphBaZGoiZf2EMjRjV2hfOV7u03G4xoU")
                .success(function (data) {
                    $scope.stores = data.rows
                    $scope.date = new Date()
                    if ($scope.date.getDay() == 0) {
                        $scope.day = 6
                    } else {
                        $scope.day = $scope.date.getDay() - 1;
                    }
                    $scope.currStore = $scope.stores[0]

                    var mapOptions = {
                        zoom: 12,
                        disableDefaultUI: true,
                        zoomControl: true,
                        center: new google.maps.LatLng($scope.lat, $scope.lng)
                    }

                    $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

                    $scope.markers = [];
                    var createMarker = function (num) {
                        info = $scope.stores[i]
                        info[3] = info[3].split(",")
                        var marker = new google.maps.Marker({
                            map: $scope.map,
                            icon: storeDir + "img/mark.png",
                            position: new google.maps.LatLng(info[3][0], info[3][1]),
                            title: info[1]
                        });

                        google.maps.event.addListener(marker, 'click', function () {
                            for (var store of $scope.stores) {
                                if (store[1] == marker.title) {
                                    $scope.selectMarker(marker, store);
                                    $scope.$apply();
                                }
                            }
                        });

                        $scope.markers.push(marker);

                    }
                    for (i = 0; i < $scope.stores.length; i++) {
                        createMarker(i);
                    }
                    var currLoc = new google.maps.Marker({
                        position: new google.maps.LatLng($scope.lat, $scope.lng),
                        map: $scope.map,
                        icon: storeDir + "img/currLocMark.png",
                        title: 'Current Location'
                    })
                })
        }
        $scope.getDirections = function (store) {
            window.open("https://www.google.com/maps?saddr=My%20Location&daddr=" + store[1] + ", " + store[2])
        }
        $scope.updateCurr = function (store) {
            for (var mark of $scope.markers) {
                if (store[1] == mark.title) {
                    $scope.selectMarker(mark, store)
                }
            }
        }
        $scope.selectMarker = function (marker, store) {
            marker.setAnimation(google.maps.Animation.BOUNCE);
            setTimeout(function () {
                marker.setAnimation(null);
            }, 1000);
            marker.setIcon(storeDir + 'img/mark-highlight.png');
            if ($scope.lastMarker) {
                $scope.lastMarker.setIcon(storeDir + "img/mark.png")
            }
            $scope.lastMarker = marker
            $scope.currStore = store
            $location.hash("details");
            $anchorScroll();
            // $scope.$apply();
        }
        $scope.$on('place_changed', function (e, place) {
            $scope.lat = place.geometry.location.lat()
            $scope.lng = place.geometry.location.lng()
            init()
        });
    })
    .filter('yesNo', function () {
        return function (input) {
            if ((input == true) || (input == "true")) {
                return ''
            } else {
                return '-tint'
            }
        }
    })
    .filter('distance', function () {
        return function (input, scope) {
            if (input) {
                var lat1 = Number(input[3][0]);
                var lng1 = Number(input[3][1]);
                var lat2 = scope.lat;
                var lng2 = scope.lng;
                var R = 3959; // miles
                var φ1 = lat1 * (Math.PI / 180);
                var φ2 = lat2 * (Math.PI / 180);
                var Δφ = (lat2 - lat1) * (Math.PI / 180);
                var Δλ = (lng2 - lng1) * (Math.PI / 180);

                var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
                    Math.cos(φ1) * Math.cos(φ2) *
                    Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                var distance = R * c
                return Math.round(distance * 10) / 10 + "mi";
            } else {
                return ""
            }
        }
    })
    .directive('googlePlace', directiveFunction);

directiveFunction.$inject = ['$rootScope'];

function directiveFunction($rootScope) {
    return {
        require: 'ngModel',
        scope: {
            ngModel: '=',
            details: '=?'
        },
        link: function (scope, element, attrs, model) {
            var options = {
                types: [],
                componentRestrictions: {
                    country: 'us'
                }
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function () {
                scope.$apply(function () {
                    scope.details = scope.gPlace.getPlace();
                    model.$setViewValue(element.val());
                    $rootScope.$broadcast('place_changed', scope.details);
                });
            });
        }
    };
}
