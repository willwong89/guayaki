<?php
/*
Page: Films
*/
get_header(); ?>

<header 
  id="page-header" 
  role="banner" 
  style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?  the_post_thumbnail_url( 'banner' ); ?>') no-repeat fixed center center/cover"
>
    <div class="content">
      <h1>Come to life</h1>
      <span>Films <?php get_template_part('/assets/images/svg/camera.svg'); ?></span>
    </div>
</header> 

<?php while ( have_posts() ) : the_post(); ?>
<section class="content-section">
  <div class="row">
    <div class="columns large-6 medium-10 small-11 intro-copy">
      <?php the_content(); ?>
    </div>
  </div>
</section>

 
<div class="row align-center">
  <div class="columns large-12 medium-10 small-12">
    <?php get_template_part( 'template-parts/film-grid' ); ?>
  </div>
</div>
 
<?php endwhile; ?>

<?php get_footer();
