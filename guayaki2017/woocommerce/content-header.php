<header id="page-header" role="banner" class="shop-header">
<img class="slide-img" data-player-id="0" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images//placeholder/jungle-canopy.jpg">
  <div class="content">
    <h1>Products</h1>
  </div>
</header> 
<div id="breadcrumb_container">
  <div class="row">
    <div class="columns small-12">
      <?php woocommerce_breadcrumb(); ?>
    </div>
  </div>
</div>