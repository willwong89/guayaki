<?php
/*
Template Name: Regeneration Business Model
*/
get_header(); ?>

<style>
  .hamburger-menu,
  .xoo-wsc-modal,
  #site-navigation,
  #footer {
    display: none;
  }

  .page-template-page-regeneration-business-model section.container {
    margin-bottom: 0;
  }
</style>

<?php ob_start(); ?>
  <nav class="rbm__nav">
    <a class="rbm__nav__back" href="#">Back</a>
    <div class="rbm__nav__container">
      <div class="rbm__nav__bg-hover"></div>
      <div class="rbm__nav__bg"></div>
      <ul class="rbm__nav__list">
        <?php
          $rbm_nav_items = [
            'Index',
            'Zero Waste',
            'Regenerative Agriculture',
            'Nurture Tradition',
            'Values-Aligned Suppliers',
            'Zero Emissions',
            'Conscious Leadership',
            'Share the Gourd'
          ];
        ?>
        <?php foreach( $rbm_nav_items as $rbm_nav_item): ?>
          <li class="rbm__nav__item" data-title="<?php echo $rbm_nav_item; ?>">
            <a class="rbm__nav__link" href="#<?php echo str_replace(' ', '', ucwords($rbm_nav_item)); ?>">
              <span class="rbm__nav__link__body" href="#">
                <span class="rbm__nav__icon"></span>
                  <span class="rbm__nav__text"><?php echo $rbm_nav_item; ?></span>
              </span>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </nav>
<?php
  $rbm_nav = ob_get_contents();
  ob_end_clean();
?>

<div class="rbm fixed" data-class="rbm fixed">

  <div class="rbm__fixed-nav">
    <div class="rbm__fixed-nav__body">
      <?php
        echo $rbm_nav;
      ?>
    </div>
  </div>

  <div class="rbm__sections">

    <section id="Index" class="rbm__section active">

      <?php
        $heading = get_field('heading');
        $content = get_field('content');
        $background_image = get_field('background_image');
        $global_impact_report = get_field('global_impact_report');
      ?>

      <?php if( $background_image ): ?>
        <div class="rbm__section__bg" style="background-image: url(<?php echo( $background_image['url'] ); ?>);"></div>
      <?php endif; ?>

      <div class="rbm__section__body">
        <div class="row align-center">
          <div class="columns small-12 medium-12 large-6">
            <div class="text-center" style="margin: 50px 0;">
              <h1><?php echo str_replace('™', '<sup>™</sup>', $heading); ?></h1>
              <div class="rbm__content"><?php echo $content; ?></div>
            </div>
          </div>
          <div class="columns small-12 medium-12 large-6">
            <?php echo $rbm_nav; ?>


            <?php if( $global_impact_report ): ?>
              <div class="text-center">
                <a
                 href="<?php echo $global_impact_report; ?>"
                 target="_blank"
                 class="btn-global-impact-report" download>
                 Global Impact Report <i class="fa fa-download" ></i>
               </a>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>

    </section>

    <?php if( have_rows('sections') ): ?>
      <?php while ( have_rows('sections') ) : the_row(); ?>
        <?php
          $heading = get_sub_field('heading');
          $background_image = get_sub_field('background_image');
          $content = get_sub_field('content');
          $accordion = get_sub_field('accordion');
        ?>

        <section id="<?php echo str_replace(' ', '', ucwords($heading)); ?>" class="rbm__section">

          <?php if( $background_image ): ?>
            <div class="rbm__section__bg" style="background-image: url(<?php echo( $background_image['url'] ); ?>);"></div>
          <?php endif; ?>

          <div class="rbm__section__body">
            <div class="row align-center">
              <div class="columns small-12 medium-12 large-12">

                <div class="rbm__box rbm__box--left">
                  <h1><?php echo $heading; ?></h1>
                  <p><?php echo $content; ?></p>
                </div>

                <?php if( $accordion ): ?>
                  <div class="rbm__box rbm__box--right">

                    <h2>Regenerative Practices</h2>

                    <div class="rbm__accordion">
                      <?php foreach( $accordion as $accordion_item ): ?>
                        <?php
                          $heading = $accordion_item['heading'];
                          $content = $accordion_item['content'];
                        ?>
                        <div class="rbm__accordion-item">
                          <h4 class="rbm__accordion-item__heading"><?php echo $heading; ?></h4>
                          <div class="rbm__accordion-item__content rte">
                            <?php if( $content != '' ): ?>
                              <?php echo $content; ?>
                            <?php endif; ?>
                          </div>
                        </div>
                      <?php endforeach; ?>
                    </div>

                    <!--
                      <p class="rbm__accordion__footer">
                        (2017 Metrics)
                      </p>
                    -->

                  </div>
                <?php endif; ?>

              </div>
            </div>
          </div>

        </section>

      <?php endwhile; ?>
    <?php endif; ?>
  </div>

</div>

<script>

  $(window).load(function() {

    for( var i=1 ; i<3 ; i++ ) {
      setTimeout(function(){
        $('.rbm__nav').addClass('hover');
        setTimeout(function(){
          $('.rbm__nav').removeClass('hover');
        }, 250);
      }, i * 500);
    }
  });


  $(window).bind('mousewheel', function(e) {

    // Always visible on #Index
    if( $('.rbm__section.active#Index').length ) {
      $('.rbm__fixed-nav').removeClass('visible');
      return;
    }

    $('.rbm__fixed-nav').addClass('visible');

    /*
    // Always visible on desktop
    if( $(window).width() >= 768 ) {

      $('.rbm__fixed-nav').addClass('visible');

    } else {

      if (e.originalEvent.wheelDelta >= 0) {
        // Scroll up
        $('.rbm__fixed-nav').addClass('visible');
      } else {
        // Scroll down
        $('.rbm__fixed-nav').removeClass('visible');
      }

    }
    */

  });


  $('body').on('click', '.rbm__fixed-nav.in', function(e) {
    if( $(e.target).closest('.rbm__nav__link').length == 0 ) {
      $('.rbm__fixed-nav.in').removeClass('in');
    }
  });


  $('body').on('click', '.rbm__nav__link', function(e) {

    var $fixedNavContainer = $(this).closest('.rbm__fixed-nav'),
        $fixedNav = $fixedNavContainer.find('.rbm__nav');

    if( $fixedNavContainer.length ) {

      if( !$fixedNavContainer.hasClass('in') ) {
        e.preventDefault();
        $fixedNavContainer.addClass('in');
        return;
      } else {
        $fixedNavContainer.removeClass('in');
      }
    }

    e.preventDefault();
    var target = $(this).attr('href');

    // Update states
    $('.rbm__section').removeClass('active');
    $(target).addClass('active');


    if( $('.rbm').hasClass('fixed') ) {
      // Always invisible on #Index
      if( $('.rbm__section.active#Index').length ) {
        $('.rbm__fixed-nav').removeClass('visible');
        return;
      }
      $('.rbm__fixed-nav').addClass('visible');
    } else {
      // Smooth scrolling
      $('html, body').animate({ scrollTop: $(target).position().top });
    }

    // Unfocus
    $(this).blur().off('hover');

  });


  $('body').on('click', '.rbm__accordion-item__heading', function(e) {

    e.preventDefault();

    var $box = $(this).closest('.rbm__box'),
        $accordionItem = $(this).closest('.rbm__accordion-item');

    if( $accordionItem.hasClass('active') ) {
      $box.removeClass('rbm__box--expanded');
      $accordionItem.removeClass('active');
    } else {
      $box.addClass('rbm__box--expanded');
      $box.find('.rbm__accordion-item').removeClass('active');
      $accordionItem.addClass('active');
    }

  });

  $('body').on('click', '.rbm__nav__back', function(e) {
    e.preventDefault();
    $('.rbm__nav__link[href="#Index"]').first().click();
    $('.rbm__nav__link[href="#Index"]').first().click();
  });
</script>
<?php get_footer();
