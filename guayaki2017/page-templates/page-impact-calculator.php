<?php
/*
Template Name: Impact Calculator
*/
get_header(); ?>

<header
  id="page-header"
  class="page-header--impact-calculator"
  role="banner"
  style="background: url('<? the_post_thumbnail_url( 'banner' ); ?>') no-repeat fixed center center/cover">
  <div class="content">
    <h1 class="guayaki-font">What's Your Personal Impact?</h1>

    <p>Discover the positive effects of your Guayaki consumption</p>

    <a href="#" class="btn-start-now white-outline-button">Start Now</a>
  </div>
</header>


<?php while ( have_posts() ) : the_post(); ?>
  <?php if( get_the_content() != '' ): ?>
    <section class="content-section">
      <div class="row">
        <div class="columns large-6 medium-10 small-11 intro-copy">
          <?php the_content(); ?>
        </div>
      </div>
    </section>
  <?php endif; ?>
<?php endwhile; ?>

<section id="impactCalculator" class="content-section impact-calculator">
  <div class="row">
    <div class="columns large-10 medium-12 small-11 intro-copy">

      <div data-impact-calculator>
        <div class="impact-calculator__step row">
          <div class="columns large-12 medium-12 small-12">
            <h2 class="impact-calculator__heading guayaki-font">
              <span class="impact-calculator__heading-num">1</span>
              Choose a Time Range
            </h2>
          </div>

          <div class="columns large-12 medium-12 small-12">
            <?php $time_ranges = ['Day', 'Week', 'Month', 'Year']; ?>
            <ul class="impact-calculator__time-range__list">
              <?php foreach( $time_ranges as $i=>$time_range ): ?>
                <li class="impact-calculator__time-range__item">
                  <input
                   type="radio"
                   id="radio-<?php echo $time_range; ?>"
                   class="impact-calculator__time-range__radio"
                   name="Time Range"
                   value="<?php echo $time_range; ?>"
                   <?php if( $i == 0 ): ?> checked="checked"<?php endif; ?>>
                  <label
                   for="radio-<?php echo $time_range; ?>"
                   class="impact-calculator__time-range__label">
                    <?php echo $time_range; ?>
                  </label>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>

        <div class="impact-calculator__step row">
          <div class="columns large-12 medium-12 small-12">
            <h2 class="impact-calculator__heading guayaki-font">
              <span class="impact-calculator__heading-num">2</span>
              Enter the Amount of Guayakí Consumed
            </h2>
          </div>

          <div class="columns large-6 medium-6 small-12 impact-calculator__field">
            <label for="can-15-5-fl-oz">Cans 15.5 fl oz</label>
            <input
             type="number"
             id="can-15-5-fl-oz"
             placeholder="Cans 15.5 fl oz"
             min="0"
             data-col="25">
          </div>

          <div class="columns large-6 medium-6 small-12 impact-calculator__field">
            <label for="gourds">Gourds</label>
            <input
             type="number"
             id="gourds"
             placeholder="Gourds"
             min="0"
             data-col="37">
          </div>

          <div class="columns large-6 medium-6 small-12 impact-calculator__field">
            <label for="sparkling-cans-12-fl-oz">Sparkling Cans 12 fl oz</label>
            <input
             type="number"
             id="sparkling-cans-12-fl-oz"
             placeholder="Sparkling Cans 12 fl oz"
             min="0"
             data-col="26">
          </div>

          <div class="columns large-6 medium-6 small-12 impact-calculator__field">
            <label for="mate-bags">Mate Bags</label>
            <input
             type="number"
             id="mate-bags"
             placeholder="Mate Bags"
             min="0"
             data-col="35">
          </div>

          <div class="columns large-6 medium-6 small-12 impact-calculator__field">
            <label for="bottles">Bottles</label>
            <input
             type="number"
             id="bottles"
             placeholder="Bottles"
             min="0"
             data-col="27">
          </div>

          <div class="columns large-6 medium-6 small-12 impact-calculator__field">
            <label for="loose-leaf-1-lb">Loose Leaf 1 lb</label>
            <input
             type="number"
             id="loose-leaf-1-lb"
             placeholder="Loose Leaf 1 lb"
             min="0"
             data-col="29">
          </div>

          <div class="columns large-6 medium-6 small-12 impact-calculator__field">
            <label for="energy-shots">Energy Shots</label>
            <input
             type="number"
             id="energy-shots"
             placeholder="Energy Shots"
             min="0"
             data-col="31">
          </div>

          <div class="columns large-6 medium-12 small-12 ">
            &nbsp;
          </div>

          <div class="columns large-12 medium-12 small-12 text-center">
            <br />
            <a href="#calculatorResults" class="big-button" data-calculator-results-modal-show>Calculate</a>
          </div>
        </div>
      </div>
    </div>

    <div class="columns large-12 medium-12 small-12">
      <table style="display:none;">
        <tr>
          <th>1 CAN 15.5oz</th>
          <td>Cans 15.5 fl oz</td>
          <td>25</td>
        </tr>
        <tr>
          <th>1 SPARKLING CAN 12oz</th>
          <td>Sparkling Cans 12 fl oz</td>
          <td>26</td>
        </tr>
        <tr>
          <th>1 TERERE BOTTLE</th>
          <td>Bottles</td>
          <td>27</td>
        </tr>
        <tr>
          <th>8oz BAG LOOSE LEAF</th>
          <td></td>
          <td>28</td>
        </tr>
        <tr>
          <th>1lb BAG or TIN LOOSE LEAF</th>
          <td>Loose Leaf 1 lb</td>
          <td>29</td>
        </tr>
        <tr>
          <th>5lb BAG LOOSE LEAF</th>
          <td></td>
          <td>30</td>
        </tr>
        <tr>
          <th>1 ENERGY SHOT</th>
          <td>Energy Shots</td>
          <td>31</td>
        </tr>
        <tr>
          <th>75 count MATE BAGS </th>
          <td></td>
          <td>32</td>
        </tr>
        <tr>
          <th>33 count MATE BAGS </th>
          <td></td>
          <td>33</td>
        </tr>
        <tr>
          <th>SINGLE MATE BAG</th>
          <td>Mate Bags</td>
          <td>35</td>
        </tr>
        <tr>
          <th>SINGLE CUP OF MATE</th>
          <td></td>
          <td>36</td>
        </tr>
        <tr>
          <th>GOURD OF MATE</th>
          <td>Gourds</td>
          <td>37</td>
        </tr>
      </table>
    </div>
  </div>
</section>

<div
 id="calculatorResults"
 class="impact-calculator__results"
 data-calculator-results-modal>
  <a class="impact-calculator__results__close" href="#" data-calculator-results-modal-close>
    <img src="<?php luc_img_dir() ?>/impact-calculator/icon-close.svg" />
  </a>

  <div class="impact-calculator__results__wrap">
    <div
     class="impact-calculator__results__sidebar text-center"
     style="background-image:url(&quot;<?php luc_img_dir() ?>/impact-calculator/bg-jungle-fog.jpg&quot;);">

     <img
      class="impact-calculator__results__logo"
      src="<?php luc_img_dir() ?>/impact-calculator/wreath-white.png" />

     <h2 class="impact-calculator__results__heading guayaki-font">Your Calculated Yearly Contribution</h2>
    </div>

    <div
     class="impact-calculator__results__content"
     style=" background-image:url(&quot;<?php luc_img_dir() ?>/impact-calculator/bg-results.jpg&quot;);">

      <div class="impact-calculator__results__content__body">
        <div data-calculator-results-table></div>

        <?php
          $result_selectors = [
            'data-no-of-trees-protected',
            'data-sq-ft-protected',
            'data-value-of-eco-system-services-provided'
          ];
          $bgs = [
            get_stylesheet_directory_uri() . '/assets/images/' . '/impact-calculator/bg-trees-protected.jpg',
            get_stylesheet_directory_uri() . '/assets/images/' . '/impact-calculator/bg-forest-protected.jpg',
            get_stylesheet_directory_uri() . '/assets/images/' . '/impact-calculator/bg-eco-system-services-provided.jpg'
          ];
          $titles = [
            'Trees <br />Protected',
            'Sq ft of Rainforests<br />Protected',
            'Of Ecosystem <br />Services Provided'
          ];
          $contents = [
            'The Araucaria angustifolia tree is a symbol of the South American Atlantic Forest and an endangered species due to its vast deforestation for timber. Remaining populations are under considerable pressure today as they are located in densely populated or agricultural areas.',
            'The South American Atlantic Forest once covered 330 million acres (twice the size of Texas), but today roughly 88% of this forest has been cleared and what remains is highly fragmented. The Forest is still among the biologically richest and most diverse forests in the world.',
            'The Forest provides local and global ecosystem services (e.g., carbon storage, climate stabilization, water supply, airborne pollutant filtering, soil nutrient cycling) valued at $816 per acre. We support a regenerative economy that integrates this into Accounting systems.'
          ];


          $trees_protected = get_field('trees_protected');
          $forest_protected = get_field('forest_protected');
          $ecosystem_services_provided = get_field('ecosystem_services_provided');

          if( $trees_protected['title'] != '' ) {
            $titles[0] = $trees_protected['title'];
          }
          if( $trees_protected['content'] != '' ) {
            $contents[0] = $trees_protected['content'];
          }
          if( $trees_protected['image']['url'] != '' ) {
            $bgs[0] = $trees_protected['image']['url'];
          }

          if( $forest_protected['title'] != '' ) {
            $titles[1] = $forest_protected['title'];
          }
          if( $forest_protected['content'] != '' ) {
            $contents[1] = $forest_protected['content'];
          }
          if( $forest_protected['image']['url'] != '' ) {
            $bgs[1] = $forest_protected['image']['url'];
          }

          if( $ecosystem_services_provided['title'] != '' ) {
            $titles[2] = $ecosystem_services_provided['title'];
          }
          if( $ecosystem_services_provided['content'] != '' ) {
            $contents[2] = $ecosystem_services_provided['content'];
          }
          if( $ecosystem_services_provided['image']['url'] != '' ) {
            $bgs[2] = $ecosystem_services_provided['image']['url'];
          }
        ?>

        <div class="impact-calculator__cards">
          <?php for( $i=0 ; $i<3 ; $i++ ): ?>
            <?php
              $result_selector = $result_selectors[$i];
              $bg = $bgs[$i];
              $title = $titles[$i];
              $content = $contents[$i];
            ?>
            <div class="impact-calculator__card">
              <div
               class="impact-calculator__card__bg"
               style=" background-image:url(&quot;<?php echo $bg; ?>&quot;);">
              </div>

              <div class="impact-calculator__card__overlay"></div>

              <div class="impact-calculator__card__body">
                <p class="impact-calculator__card__result"><span <?php echo $result_selector; ?>></span></p>

                <hr>

                <h4 class="impact-calculator__card__title">
                  <?php echo $title; ?>
                </h4>

                <div class="impact-calculator__card__content">
                  <?php echo $content; ?>
                </div>
              </div>
            </div>
          <?php endfor; ?>
        </div>

        <div class="impact-calculator__results__share">
          <p>Share the results with your friends!</p>

          <!-- AddToAny BEGIN -->
          <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
          </div>
          <script async src="https://static.addtoany.com/menu/page.js"></script>
          <!-- AddToAny END -->
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var Calculator = {
    devMode: false,
    selectors: {
      impactCalculator: '[data-impact-calculator]',
      numberInput: '[data-impact-calculator] [type="number"]',
      timeRangeInput: '[data-impact-calculator] [name="Time Range"]'
    },
    matePerAcre: {},
    results: {},
    init: function() {
      var scope = this,
          selectors = this.selectors;

      this.initMatePerAcre(function() {
        $(selectors.numberInput + ', ' + selectors.timeRangeInput).change(function(){
          var val = parseFloat( $(this).val() );
          if( val < 0 ) $(this).val(0);
          scope.calculateResults(scope);
          scope.updateFieldLabel(scope);

          serializedData = scope.serializeCalculator();
          console.log(serializedData);

          scope.updateUrlParameter(serializedData);
        });
        scope.populateFormDataFromUrl();
      });

      $('[data-calculator-results-modal-show]').click(function(e){
        e.preventDefault();
        var target = $(this).attr('href');
        $(target).addClass('in');

        history.pushState({}, null, location.pathname + location.search + '&action=showResult' );
      });

      $('[data-calculator-results-modal]').on('click', function(e) {
        e.preventDefault();
        if( !$(e.target).closest('.impact-calculator__results__wrap').length ) {
          $(this).removeClass('in');
        }
      });

      $('[data-calculator-results-modal-close]').on('click', function(e){
        e.preventDefault();
        $('[data-calculator-results-modal]').removeClass('in');
      });

      $('.impact-calculator__cards').slick({
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      })
    },
    initMatePerAcre: function( callback ) {
      var Calculator = this,
          matePerAcre = {},
          key = '1Ck7CyYRHGxkuaLvso9Ibn7dxD52580TJduCLmGUysYw',
          worksheetId = 2, // 1 - 3
          jsonFeed = 'https://spreadsheets.google.com/feeds/cells/' + key +'/' + worksheetId + '/public/basic?alt=json',
          targetColumns = 'BCDEFGHIJ';

      $.get(jsonFeed, function(data) {
        var feed = data.feed,
            title = feed.title,
            entry = feed.entry;

        for( var i=0 ; i<entry.length ; i++ ) {
          var entryItem = entry[i],
              col = entryItem.title.$t.charAt(0),
              row = parseInt( entryItem.title.$t.substring(1) ),
              content = entryItem.content.$t;

          if( targetColumns.indexOf(col) != -1 &&
              (row >= 25 && row <= 37 ) ) {
            matePerAcre[entryItem.title.$t] = content;
          }
        }

        Calculator.matePerAcre = matePerAcre;

        if( typeof callback === "function" ) {
          callback();
        }
      });
    },
    calculateResults: function(scope) {
      var Calculator = scope;

      var results = {
        total: {
          title: 'Total',
          beveragesConsumed: 0,
          acresProtected: 0,
          sqFtProtected: 0,
          noOfTressProtected: 0,
          valueOfEcosystemServicesProvided: 0
        }
      };

      var matePerAcre = Calculator.matePerAcre,
          timeRange = $('.impact-calculator__time-range__radio:checked').val(),
          multiplier = 365;

      if( timeRange == 'Week' ) {
        multiplier = 52;
      } else if( timeRange == 'Month' ) {
        multiplier = 11.967;
      } else if( timeRange == 'Year' ) {
        multiplier = 1;
      }

      $(Calculator.selectors.numberInput).each(function(){
        var id = $(this).attr('id'),
            col = $(this).attr('data-col'),
            title = $(this).closest('.columns').find('label').html(),
            beveragesConsumed = parseFloat( $(this).val() );

        beveragesConsumed = isNaN(beveragesConsumed) ? 0 : beveragesConsumed;
        beveragesConsumed = (beveragesConsumed <0) ? 0 : beveragesConsumed;
        beveragesConsumed *= multiplier;

        var result = {
          title: title,
          beveragesConsumed: beveragesConsumed,
          acresProtected: beveragesConsumed * parseFloat( matePerAcre['G'+col] ),
          sqFtProtected: beveragesConsumed * parseFloat( matePerAcre['H'+col] ),
          noOfTressProtected: beveragesConsumed * parseFloat( matePerAcre['I'+col] ),
          valueOfEcosystemServicesProvided: beveragesConsumed * parseFloat( matePerAcre['J'+col].replace('$', '') )
        };

        // push to results object
        results[id] = result;

        // update total
        results.total.beveragesConsumed += result.beveragesConsumed;
        results.total.acresProtected += result.acresProtected;
        results.total.sqFtProtected += result.sqFtProtected;
        results.total.noOfTressProtected += result.noOfTressProtected;
        results.total.valueOfEcosystemServicesProvided += result.valueOfEcosystemServicesProvided;
      });

      Calculator.results = results;
      Calculator.roundUpResults();
      Calculator.printResults();
    },
    roundUpResults: function() {
      var results = this.results;
      for (var key in results) {
        if ( results.hasOwnProperty(key) ) {
          var result = results[key];
          if( result.title == 'Total' ) {
            result.acresProtected = Math.round( result.acresProtected );
          } else {
            result.acresProtected = result.acresProtected.toFixed(2);
          }
          result.sqFtProtected = Math.round( result.sqFtProtected );
          result.noOfTressProtected = Math.round( result.noOfTressProtected );
          result.valueOfEcosystemServicesProvided = Math.round( result.valueOfEcosystemServicesProvided );
        }
      }
    },
    printResults: function() {
      var results = this.results,
          toNumberString = this.toNumberString;

      $('[data-no-of-trees-protected]').html( toNumberString( results.total.noOfTressProtected ) );
      $('[data-sq-ft-protected]').html( toNumberString( results.total.sqFtProtected ) );
      $('[data-value-of-eco-system-services-provided]').html( '$'+toNumberString( results.total.valueOfEcosystemServicesProvided ) );

      if( !this.devMode ) {
        return;
      }

      var resultsHtml = '\
        <table>\
          <tr>\
            <th>Title</th>\
            <th>beveragesConsumed</th>\
            <th>acresProtected</th>\
            <th>sqFtProtected</th>\
            <th>noOfTressProtected</th>\
            <th>valueOfEcosystemServicesProvided</th>\
          </tr>';

      for (var key in results) {
        if (results.hasOwnProperty(key) && key != 'total') {
          var result = results[key];
          if( result.beveragesConsumed == 0 ) {
            continue;
          }

          resultsHtml += '\
            <tr>\
              <td>' + result.title + '</td>\
              <td>' + result.beveragesConsumed + '</td>\
              <td>' + result.acresProtected + '</td>\
              <td>' + result.sqFtProtected + '</td>\
              <td>' + result.noOfTressProtected + '</td>\
              <td>' + result.valueOfEcosystemServicesProvided + '</td>\
            </tr>';
        }
      }

      resultsHtml += '\
        <tr>\
          <td>' + results.total.title + '</td>\
          <td>' + results.total.beveragesConsumed + '</td>\
          <td>' + results.total.acresProtected + '</td>\
          <td>' + results.total.sqFtProtected + '</td>\
          <td>' + results.total.noOfTressProtected + '</td>\
          <td>' + results.total.valueOfEcosystemServicesProvided + '</td>\
        </tr>';

      resultsHtml += '</table>';
      $('[data-calculator-results-table]').html(resultsHtml);
    },
    updateFieldLabel: function(scope) {
      $(scope.selectors.numberInput).each(function(){
        var val = $(this).val(),
            $fieldWrap = $(this).closest('.impact-calculator__field');

        if( val == '' ) {
          $fieldWrap.removeClass('show-floating-label');
        } else {
          $fieldWrap.addClass('show-floating-label');
        }
      });
    },
    updateUrlParameter: function(serializedData) {
      var paramData = serializedData;
      for (var key in serializedData) {
        if (serializedData.hasOwnProperty(key)) {
          if( serializedData[key] != '' ) {
            paramData[key] = serializedData[key];
          } else {
            delete paramData[key];
          }
        }
      }
      history.pushState({}, null, location.pathname + '?' + $.param(paramData) );
    },
    toNumberString: function( s ){
      // https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
      return s.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    },
    serializeCalculator: function() {
      var params = {};
      params.timeRange = $('[data-impact-calculator] [name="Time Range"]:checked').val();

      $('[data-impact-calculator] [type="number"]').each(function(){
        var id = $(this).attr('id'),
            val = $(this).val();
        params[id] = val;
      });
      return params;
    },
    populateFormDataFromUrl: function() {

      // https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js

      var sPageURL = window.location.search.substring(1),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i,
          action;

      for( var i=0 ; i<sURLVariables.length ; i++ ) {
        sURLVariable = sURLVariables[i].split('=');

        if( sURLVariable[0] == 'timeRange' ) {
          $('.impact-calculator__time-range__item').find('[value="' + sURLVariable[1] + '"]').prop('checked', 'checked');
        } else if( sURLVariable[0] == 'action' ){
          action = sURLVariable[1];
        } else {
          var $input = $('#'+sURLVariable[0]);
          if( $input.length ) {
            $input.val(sURLVariable[1]);
          }
        }
      }

      $('.impact-calculator__field').find('[type="number"]').first().change();

      if( action == 'showResult' ) {
        $('[data-calculator-results-modal-show]').click();
      }
    }
  };

  $(document).ready(function(){
    Calculator.init();

    $('.btn-start-now').click(function(e){
      e.preventDefault();
      $('html, body').animate({
        scrollTop: $('#impactCalculator').offset().top
      }, 500, 'linear');
    });

  });
</script>

<?php get_footer();
