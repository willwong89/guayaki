<?php
/*
Template Name: Artists/Come To Life home
*/
get_header(); ?>
 <?php while ( have_posts() ) : the_post(); ?>
<header id="page-header" role="banner">
    <?php the_post_thumbnail('full'); ?>
    <div class="content">
      <h1>Come to life</h1>
      <span>Music <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/music-note.png"/></span>
    </div>
</header> 
 
<section  class="content-section artist-overview -slim -guay">
  <div class="row align-center">
    <div class="columns large-7 medium-9 small-12">
      <?php the_content(); ?>
  </div>
</section>
<?php endwhile; ?>

<section  class="content-section artists -slim">
  <div class="row align-center">
    <div class="columns large-10 small-12 artist-grid">
      <?php $the_query = new WP_Query( array( 'posts_per_page' => 30, 'post_type' => 'artist', 'orderby' => 'menu_order', 'order' =>'ASC') ); ?>
      <?php if ( $the_query->have_posts() ) : ?>
          <?php while ( $the_query->have_posts() ) : $the_query->the_post();  ?>
            <a href="<?php the_permalink(); ?>" class="single-artist"><?php the_post_thumbnail('artist'); ?><span><?php the_title(); ?></span></a>
          <?php endwhile; ?>
      <?php endif; ?>
      <? wp_reset_query(); ?>
  </div>
</section>


<?php get_footer();
