<?php
/*
Template Name: Front
*/
get_header(); ?>

	<section id="featured-films" class="page-header" role="banner">
		<div id="film-orbit" class="orbit" data-orbit data-pause-on-hover="true" data-next-on-click="false" data-auto-play="true" 
		data-resume-on-mouseout="false" data-timer-speed="20000" role="region" aria-label="Featured Films" >
		  <ul class="orbit-container">
		    <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><span class="g-caret"></span></button>
		    <button class="orbit-next"><span class="show-for-sr">Next Slide</span><span class="g-caret"></button>
		     <?php if(have_rows('carousel' )) : ?> 
	          <?php while( have_rows('carousel')) : the_row();   ?>
	  
	            <?php $image = get_sub_field('image'); ?>
			        <li class="orbit-slide" data-player-id="0">
				    		<img class="slide-img desktop-slide" data-player-id="0" src="<?php echo $image['sizes']['banner']?>" />
					   	 	<img class="slide-img mobile-slide" data-player-id="0" src="<?php echo $image['sizes']['banner-mobile']?>" />
			 		    	<div class="content">
									<h2><a class="swipebox-video" href="<?php the_sub_field('video_url');?>"><?php the_sub_field('title');?></a></h2>
							 		<a class="swipebox-video video-play-button" href="<?php the_sub_field('video_url');?>"></a> 
						 		</div> 
				    	</li>          
	        <?php endwhile; endif; ?>
	 		</ul>

		  <nav class="orbit-bullets">
		  	<?php if(have_rows('carousel' )) : ?> 
	       	<?php $i = 0; ?>
	       	<?php while( have_rows('carousel')) : the_row();   ?>
	      	  	<button class="is-active" data-slide="0">
	      	  		<span class="show-for-sr">Slide <?php echo $i; ?> details.</span>
	      	  	</button>
	      			<?php $i++;  ?>
	    		<?php endwhile; endif; ?>
	  	</nav>
		</div>

	</section>
<?php while ( have_posts() ) : the_post(); ?>
	
	<section id="our-vision" class="content-section -bg -fat">
		<div class="row">
			<div class="columns  large-7 medium-9 small-12">
				<h1 class="guayaki-font">Our Vision</h1>
				<p><?php the_field('vision') ?></p>
				<a href="/vision"  class="big-button">Learn More</a>
			</div>
		</div>
	</section>

	<section id="front-intro" class="content-section">
		<div class="row align-center">
			<div class="columns large-7 medium-9 small-12">
				<?php the_content(); ?>
		</div>
	</section>


	<section id="home-music-grid" class="content-section -slim">
	 <div class="row">
		<h2 class="icon-header">Music <?php get_template_part('/assets/images/svg/music-note.svg'); ?></h2>
			<div class="columns large-12 small-12 artist-grid">
		  	<a href="http://cometolife.com" class="single-artist" target="_blank"><img src="http://cometolife.com/wp-content/uploads/2017/07/GYM_CTL_ARTISTS_RisingAppalachia2-522x722.jpg" class="attachment-artist size-artist wp-post-image" alt=""><span>Rising Appalachia</span></a>
		    <a href="http://cometolife.com" class="single-artist" target="_blank"><img src="http://cometolife.com/wp-content/uploads/2017/07/GYM_CTL_ARTISTS_DustinThomas-1-522x722.jpg" class="attachment-artist size-artist wp-post-image" alt=""><span>Dustin Thomas</span></a>
		    <a href="http://cometolife.com" class="single-artist" target="_blank"><img src="http://cometolife.com/wp-content/uploads/2017/07/GYM_CTL_ARTISTS_Peia2-1-522x722.jpg" class="attachment-artist size-artist wp-post-image" alt=""><span>Peia</span></a>
		    <a href="http://cometolife.com" class="single-artist" target="_blank"><img src="http://cometolife.com/wp-content/uploads/2017/07/GYM_CTL_ARTISTS_Masauko2-522x722.jpg" class="attachment-artist size-artist wp-post-image" alt=""><span>Masauko</span></a>
		  </div>
	  </div>
	</section>




	<section id="connect-with-us" class="content-section -fat">
		<div class="row align-center">
			<div class="columns large-7 medium-9 small-12">
				<h1 class="guayaki-font"><a href="/contact">Connect With Us</a></h1>
				<p>Receive the latest stories — We will send you new films, exclusive articles and other info on a monthly basis.</p>
				<?php get_template_part('template-parts/connect-group'); ?>
		</div>
	</section>

	<section id="amigos" class="content-section -bg -fat">
		<div class="row">
			<div class="columns  large-7 medium-9 small-12">
				<h1 class="guayaki-font" >Meet Our <br/ >Amigos De Guayak&iacute;</h1>
				<a href="/amigos" class="big-button">Enter</a>
			</div>
		</div>
	</section>

	<section id="become-a-univeristy-cebador" class="content-section -fat">
		<div class="row">
			<div class="columns  large-7 medium-9 small-12">
				<h1  class="guayaki-font" >Become a University Ambacebador</h1>
				<p>Join our team of adventurous, engaging &amp; creative ambacebadores.</p>
				<a href="/become-a-university-ambacebador" class="big-button">Get Involved</a>
			</div>
		</div>
	</section>

	<section id="yerba-mate" class="content-section -bg -fat ">
		<div class="row">
			<div class="columns  large-7 medium-9 small-12">
				<h1 class="guayaki-font">Yerba Mate</h1>
				<a href="yerba-mate" class="big-button">Explore</a>
			</div>
		</div>
	</section>
<?php endwhile; ?>





<!--
<section id="amigos" class="content-section ">
	<div class="row">
		<div class="columns large-9 medium-11 small-12">
		<h1>Amigos</h1>
		<p>Meet Our Amigos de Guayaki</p>
		
		<div class="amigo-slider">
			<a class="single-amigo"><img src="<?php luc_img_dir(); ?>placeholder/team-1.jpg" /><span>Chloe Smith</span></a>
			<a class="single-amigo"><img src="<?php luc_img_dir(); ?>placeholder/team-2.jpg" /><span>Jared Allan</span></a>
			<a class="single-amigo"><img src="<?php luc_img_dir(); ?>placeholder/team-3.jpg" /><span>Kassia Meador</span></a>
			<a class="single-amigo"><img src="<?php luc_img_dir(); ?>placeholder/team-1.jpg" /><span>Chloe Smith</span></a>
			<a class="single-amigo"><img src="<?php luc_img_dir(); ?>placeholder/team-2.jpg" /><span>Jared Allan</span></a>
			<a class="single-amigo"><img src="<?php luc_img_dir(); ?>placeholder/team-3.jpg" /><span>Kassia Meador</span></a>
		</div>

		<a class="big-button">Enter</a>
		</div>
	</div>
</section>-->
<?php get_footer();


