<?php
/*
Template Name: Amigos Archive
*/
get_header(); ?>
 <?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'template-parts/featured-image' ); ?>
 
<?php endwhile; ?>


<section  class="content-section -slim">
  <div class="row align-center">
     <div class="columns small-12 large-9">
      <?php the_content(); ?>
     </div>
  </div>
</section>
<section  class="content-section amigos -slim">
  <div class="row align-center">
    <div class="columns small-12 large-12 amigo-grid">
      <?php $the_query = new WP_Query( array( 'posts_per_page' => 30, 'post_type' => 'amigo', 'orderby' => 'menu_order', 'order' =>'ASC') ); ?>
      <?php if ( $the_query->have_posts() ) : ?>
          <?php while ( $the_query->have_posts() ) : $the_query->the_post();  ?>
            <a href="<?php the_permalink(); ?>" class="single-amigo"><?php the_post_thumbnail('artist'); ?><span><?php the_title(); ?></span></a>
          <?php endwhile; ?>
      <?php endif; ?>
      <? wp_reset_query(); ?>
  </div>
</section>


<?php get_footer();
