<?php
/*
Page: FAQ
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<section  class="content-section -slim">
  <div class="row align-center">
    <div class="columns large-10 medium-10 small-12">
    <?php the_content(); ?>

    <?php if( have_rows('faq') ):
      echo ' <ul id="accordion" class="accordion faq" data-accordion role="tablist">';
      
      $i = 0; 

      while ( have_rows('faq') ) : the_row(); $i++; ?>

       <li class="accordion-item">
          <!-- The tab title needs role="tab", an href, a unique ID, and aria-controls. -->
          <a 
            href="#panel1" 
            role="tab" class="accordion-title" 
            id="panel1d-heading" 
            aria-controls="panel1d">
          <?php the_sub_field('question'); ?>
        </a>
          <!-- The content pane needs an ID that matches the above href, role="tabpanel", data-tab-content, and aria-labelledby. -->
          <div id="panel1d" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
           <?php the_sub_field('answer'); ?>
          </div>
        </li>

      <?php endwhile;
      echo "</ul>";

    endif; ?>
              <!-- Accordion -->
         
            
         
          
  </div>
</section>
 
<?php get_footer();
