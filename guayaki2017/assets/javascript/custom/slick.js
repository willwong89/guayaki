$(document).ready(function(){ 
  $('.artist-grid').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    prevArrow:'<button class="slick-scroll slick-previous"></button>',
    nextArrow:'<button class="slick-scroll slick-next"></button>',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          arrows: false,
          slidesToShow: 4,
          slidesToScroll: 4,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          arrows: true,
          infinite: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          variableWidth: true,
          centerMode: true,
          arrows: true,
          infinite: true,
        }
      } 
    ]
  });

  $('.slick-amigo-gallery').slick({
    adaptiveHeight: true,
    prevArrow:'<button class="slick-scroll slick-previous"><span class="g-caret"></span></button>',
    nextArrow:'<button class="slick-scroll slick-next"><span class="g-caret"></span></button>',
  });

  $('.film-grid').slick({
    slidesToShow: 3,
    arrows: false,
    dots: false,
    infinite: false,
    prevArrow:'<button class="slick-scroll slick-previous"></button>',
    nextArrow:'<button class="slick-scroll slick-next"></button>',
     responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true
      }
    }]
  })
})