  //Global Vars
var isMobile; 

$(document).ready(function() {

  //setup Mobile variable 
  var mediaQueryQuery = window.matchMedia("only screen and (max-width: 760px)");
  isMobile = (mediaQueryQuery.matches) ? true : false;   


  //Nav Icon Animation 
  $('#nav-icon, .hamburger-menu').on('click', function() {
    $(this).find('.bar').toggleClass('animate');
    $('#menu').toggleClass('open');
    $('html, body').toggleClass('menu-open');
    return false; 
  });

  //Form Clear 
  $('input, texarea').on('focus', function(){
    //$(this).attr('placeholder', ''); 
  });
  
  //Swipebox
  $('.swipebox-video' ).swipebox( {
    useCSS : true, // false will force the use of jQuery for animations
    useSVG : true, // false to force the use of png for buttons
    initialIndexOnArray : 0, // which image index to init when a array is passed
    hideCloseButtonOnMobile : false, // true will hide the close button on mobile devices
    removeBarsOnMobile : true, // false will show top bar on mobile devices
    hideBarsDelay : 3000, // delay before hiding bars on desktop
    videoMaxWidth : 1140, // videos max width
    beforeOpen: function() {}, // called before opening
    afterOpen: null, // called after opening
    afterClose: function() {}, // called after closing
    loopAtEnd: false // true will return to the first image after the last image is reached
  } );

  //Header Scroll Behavior
  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  var navbarHeight = $('#site-navigation').outerHeight();

  $(window).scroll(function(event){
      //didScroll = true;
  });

  /*setInterval(function() {
      if (didScroll) {
          hasScrolled();
          didScroll = false;
      }
  }, 250);
 */
  function hasScrolled() {
      var st = $(window).scrollTop();
    
      // Make sure they scroll more than delta
      if(Math.abs(lastScrollTop - st) <= delta) { 
          return;
      }
      // If they scrolled down and are past the navbar, add class .nav-up.
      // This is necessary so you never see what is "behind" the navbar.
      if (st > lastScrollTop && st > navbarHeight){
          // Scroll Down
          $('#site-navigation').removeClass('nav-down').addClass('nav-up');
      } else {
          // Scroll Up
          if(st + $(window).height() < $(document).height()) {
              $('#site-navigation').removeClass('nav-up').addClass('nav-down');
          }
      }
      
      lastScrollTop = st;
  }


//Featured Films only slickify on mobile



});