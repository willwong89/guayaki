 <svg version="1.1" id="facebook_square" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 97 97" style="enable-background:new 0 0 97 97;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
</style>
<g>
	<g>
		<path class="st0" d="M40.2,32.3v10h-7.3v12.2h7.3v36.3h15V54.5h10.1c0,0,0.9-5.9,1.4-12.3H55.3v-8.3c0-1.2,1.6-2.9,3.3-2.9h8.2
			V18.3H55.6C39.8,18.3,40.2,30.5,40.2,32.3L40.2,32.3z M40.2,32.3"/>
	</g>
	<path class="st0" d="M48.5,7C71.4,7,90,25.6,90,48.5S71.4,90,48.5,90S7,71.4,7,48.5S25.6,7,48.5,7 M48.5,0C21.7,0,0,21.7,0,48.5
		C0,75.3,21.7,97,48.5,97C75.3,97,97,75.3,97,48.5C97,21.7,75.3,0,48.5,0L48.5,0z"/>
</g>
</svg>
