<svg version="1.1" id="music-note" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 47.3 34.1" style="enable-background:new 0 0 47.3 34.1;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
</style>
<g>
	<g>
		<rect x="14.9" class="st0" width="32.3" height="2.3"/>
	</g>
	<path class="st0" d="M47.3,25.2V7.5h0V7.5H14.9v0.1h0v12.6c-1.6-1.8-3.9-3-6.5-3c-4.7,0-8.5,3.8-8.5,8.5s3.8,8.5,8.5,8.5
		s8.5-3.8,8.5-8.5c0-0.1,0-0.3,0-0.4V9.8h28.4v10.4c-1.6-1.8-3.9-3-6.5-3c-4.7,0-8.5,3.8-8.5,8.5s3.8,8.5,8.5,8.5s8.5-3.8,8.5-8.5
		C47.3,25.5,47.3,25.3,47.3,25.2z M8.5,32.1c-3.6,0-6.5-2.9-6.5-6.5s2.9-6.5,6.5-6.5c3.6,0,6.5,2.9,6.5,6.5S12,32.1,8.5,32.1z
		 M38.8,32.1c-3.6,0-6.5-2.9-6.5-6.5s2.9-6.5,6.5-6.5c3.6,0,6.5,2.9,6.5,6.5S42.4,32.1,38.8,32.1z"/>
</g>
</svg>
