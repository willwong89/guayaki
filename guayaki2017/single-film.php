<?php
/*
Template Name: Single Film 
*/
get_header(); ?>

<header id="main-video" role="banner">
   <iframe src="https://player.vimeo.com/video/153954212" wispanh="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</header> 

   <div class="film-content">
     <h1><?php the_title(); ?></h1>
     <p>Sed perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, rem aperiam, eaque ipsa quae ab illo inventore veritatis et dolores eos qui ration quasi architecto beatae vitae dicta sunt explicabo. atem accusantium doloremque laudantium, rem aperiam, eaque ipsa quae ab illo in. Dantium, rem apnde omnis iste natus vitaedarchit error sit voluptatem accusantiun an heok quasi architecto beata.</p>

    <ul class="credits"> 
      <li><span>Director: </span>Ian Durkin</li>
      <li><span>Producer: </span>Michael Clarke</li>
      <li><span>Music: </span>Kevin Steen</li>
      <li><span>Special Thanks: </span>Jackson Lynch</li>
    </ul>

  </div>

  <hr /> 
  
<?php get_template_part('template-parts/content','film-grid'); ?> 

<?php get_footer();
