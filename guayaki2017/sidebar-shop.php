<?php
/**
 * The sidebar containing the main widget area
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<aside class="shop-sidebar">
  <?php wp_nav_menu( array('menu_class' => 'shop_menu', 'theme_location' => 'shop-nav' )); ?>

  <?php if(is_user_logged_in()) : ?>
    
    <span class="user-welcome">Welcome, <br /><?php echo wp_get_current_user()->display_name; ?></span>
    
    <a class="logout-link" href="<?php echo wp_logout_url(); ?>">Log out</a>

  <?php else: ?>
    <a class="logout-link" href="<?php echo wp_login_url(); ?>">Log in</a>
  <?php endif; ?>

</aside>
