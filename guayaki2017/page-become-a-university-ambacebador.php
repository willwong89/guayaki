<?php
/*
Page: Become A University Cebador
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="section-1" class="content-section">
  <div class="row align-center">
    <div class="columns large-10 medium-10 small-12">
      <?php the_field("content_section_1") ?>
    </div>
  </div>
</section>

<section id="section-2" class="content-section -bg -fat">
  <div class="row">
    <div class="columns small-12 medium-9 large-6">
      <img id="ambacebador-arrow" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/yellow-arrow.png"/>
      <?php the_field("content_section_2") ?>
    </div>  
</section>

<section id="section-3" class="content-section ">
  <div class="row">
    <div class="columns small-12 medium-9 large-7">
      <?php the_field("content_section_3") ?>
    </div>  
</section>

<?php endwhile; ?>

<?php get_footer();
