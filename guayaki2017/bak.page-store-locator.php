<!--<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>Store Locator</title>
    <link rel="icon" href="img/mark.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?php $storeDir =  get_stylesheet_directory_uri() . "/store-locator/" ?>
    <link href="<?php echo $storeDir ?>styles.css" rel="stylesheet">
</head>-->

<?php get_header(); ?>

<header id="page-header" role="banner">
  <img class="slide-img" data-player-id="0" src="<?php luc_img_dir() ?>/placeholder/jungle-canopy.jpg" />
  <div class="content">
  </div>
</header> 


<div id="guay-locator" ng-app="mapsApp" ng-controller="MapCtrl">
    
    <div id="list-container"> 
        <div id="list">
            <div class="listStore" id="details">
                <p class="distance">{{currStore | distance:this}}</p>
                <p class="title">{{currStore[1]}}</p>
                <p class="address">{{currStore[2]}}</p>
                <br>
                <a class="directions" href="" ng-click="getDirections(currStore)">Get Directions</a>
                <div class="currProducts">
                    <div class="product">
                        <img ng-src="<?php echo $storeDir ?>img/bottle{{currStore[11] | yesNo}}.png" width="100%" />
                    </div>
                    <div class="product">
                        <img ng-src="<?php echo $storeDir ?>img/can{{currStore[12] | yesNo}}.png" width="100%" />
                    </div>
                    <div class="product">
                        <img ng-src="<?php echo $storeDir ?>img/shot{{currStore[13] | yesNo}}.png" width="100%" />
                    </div>
                    <div class="product">
                        <img ng-src="<?php echo $storeDir ?>img/loose{{currStore[14] | yesNo}}.png" width="100%" />
                    </div>
                </div>
            </div>
            
            <hr>
            <div class="stores" ng-repeat="store in stores">
                <div class="listStore" ng-if="store[0]!=currStore[0]" ng-attr-id="{{store[0]}}" ng-click="updateCurr(store)">
                    <p class="distance">{{store | distance:this}}</p>
                    <p class="title">{{store[1]}}</p>
                    <p class="address">{{store[2]}}</p>
                </div>
            </div>
        </div>
    </div>
    <input type="text" class="controls" google-place ng-model="venue.address_line_1" placeholder="Find Guayaki Yerba Mate near you">
    
    <div id="map"></div>
</div>
 
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
<script src="<?php echo $storeDir ?>storesAngular.js"></script>

<?php get_footer(); ?>