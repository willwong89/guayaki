<?php
/**
 * Lucent Helper Functions 
 *
 */


//Shorthand To Images directory
function luc_img_dir() {

  $dir = get_stylesheet_directory_uri() . '/assets/images/'; 

  echo $dir; 
}


//Add Page Slug as Body Class
function add_slug_body_class( $classes ) {
  global $post;
  
  if ( isset( $post ) ) {
    $classes[] = $post->post_type . '-' . $post->post_name;
  }
  
  return $classes;
}

add_filter( 'body_class', 'add_slug_body_class' );

?>