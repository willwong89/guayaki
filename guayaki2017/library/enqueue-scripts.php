<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

function admin_style() {
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/assets/stylesheets/admin.css', _FILE_);
}

add_action('login_enqueue_scripts', 'admin_style');


if ( ! function_exists( 'foundationpress_scripts' ) ) :
	function foundationpress_scripts() {

	// Enqueue the main Stylesheet.
	wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/assets/stylesheets/foundation.css', array(), '2.6.1', 'all' );
  

	// Deregister the jquery version bundled with WordPress.
	wp_deregister_script( 'jquery' );

	// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );

	
	//wp_enqueue_script( 'jquery-mobile', 'http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js', array('jquery'), '1.4.5', false );

	//Vimeo Player.js 
  wp_enqueue_script( 'vimeo-player',  'https://player.vimeo.com/api/player.js', array('jquery'), '1.1', true );
 
  //Slick
  wp_enqueue_script( 'slick',  'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array('jquery'), '1.1', true );

  wp_enqueue_script( 'googlemaps',  'https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false',  '', '1.1', true );

  wp_enqueue_script( 'angular',  'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js', '', '1.1', true );
  
  wp_enqueue_script( 'materialize',  'https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js','', '1.1', true );


	// If you'd like to cherry-pick the foundation components you need in your project, head over to gulpfile.js and see lines 35-54.
	// It's a good idea to do this, performance-wise. No need to load everything if you're just going to use the grid anyway, you know :)
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/javascript/foundation.js', array('jquery'), '2.6.1', true );
 


	// Add the comment-reply library on pages where it is necessary
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;
