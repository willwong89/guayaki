<?php
/**
 * Override Woocommerce
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

  remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

  add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );

  function jk_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' &#47; '
        );
  }

?>