<?php
/*
Single Amigo Page
*/
get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
<?php $banner = get_field('banner_image'); ?>
 
  <header 
    id="page-header" 
    role="banner" 
    style="background:linear-gradient(rgba(0, 0, 0, 0.4) 10%, transparent 100%), url(<?php  echo $banner['sizes']['banner'] ?>) center  / cover no-repeat fixed;">
  </header>
  

    <section class="content-section amigo-bio -slim">
      <div class="row align-center">
        <div class="columns small-12 amigo-bio-header">
          <h1><?php the_title(); ?></h1>
          <span class="location"><?php the_field('location'); ?></span>
        </div>
        <div class="columns medium-6 small-12">
          <?php the_post_thumbnail('large'); ?>
        </div>
        <div class="columns  medium-6 small-12">
          
          <?php the_content(); ?>
          
          <div class="social-links">
          <h5>Follow <?php the_title(); ?></h5>

          <?php if(get_field('ambassador_site')) : ?>
              <a target="_blank" href="<?php the_field('ambassador_site'); ?>"><?php get_template_part('/assets/images/svg/artist-site.svg'); ?></a>
            <?php endif; ?>

            <?php if(get_field('twitter_link')) : ?>
              <a target="_blank" href="<?php the_field('twitter_link'); ?>"><?php get_template_part('/assets/images/svg/twitter.svg'); ?></a>
            <?php endif; ?>

            <?php if(get_field('instagram_link')) : ?>
             <a target="_blank" href="<?php the_field('instagram_link'); ?>"><?php get_template_part('/assets/images/svg/instagram.svg'); ?></a>
            <?php endif; ?>

            <?php if(get_field('facebook_link')) : ?>
              <a target="_blank" href="<?php the_field('facebook_link'); ?>"><?php get_template_part('/assets/images/svg/facebook.svg'); ?></a>
            <?php endif; ?>
         </div>
        </div>
      
    </section>


    <?php 

      $images = get_field('image_gallery');

      if( $images ): ?>
        <section class="content-section amigo-gallery row align-center">
          <div class="columns small-12">
            <div class="slick-amigo-gallery grid-col-1">
              <?php foreach( $images as $image ): ?>       
                <div><?php echo wp_get_attachment_image( $image['ID'], 'banner' ); ?></div>
              <?php endforeach; ?>
           </div>
         </div>
         </section>
      <?php endif; ?>

    <?php if(get_field('recipe_title') != "") : ?>
    <section class="content-section recipe">
      <div class="row">
        <div class="columns small-12 recipe-header">
          <h2 class="icon-header">Recipe</h2>
          <p><?php the_field('description'); ?></p>
          <?php $recipe_image = get_field('recipe_image'); ?>
          <img src="<?php echo $recipe_image['sizes']['fp-large'] ?>" />
        </div>
        <div class="columns small-12 medium-4 recipe-sidebar">
          <h3><?php the_field('recipe_title'); ?></h3>
        </div>
        <div class="columns small-12 medium-8">
          <?php if( have_rows('recipe_steps') ):
            echo '<ol class="recipe-steps">';
            
            while ( have_rows('recipe_steps') ) : the_row(); ?>

               <li><?php the_sub_field('step_instructions'); ?></li>

           <?php endwhile;
           echo "</ol>";

          endif; ?>
        </div>
      </div>
      
      <div>

    </section>
  <?php endif; ?>
  <?php endwhile; ?>
<?php endif; ?>
<?php get_footer();
