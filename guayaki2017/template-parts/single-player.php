<div class="scroll-text">
  <p><?php echo get_the_title() . ' &mdash; ' .  get_sub_field('title') ?></p>
</div>

<?php $file = get_sub_field('mp3_file'); ?>

<?php if($file != '') : ?>

  <div class="player-container">
    <audio class="music-player" preload="auto" src="<?php echo $file['url']?>"></audio> 
    <button class="play-button"></button>
    <div class="play-bar"><span class="static-line"></span><canvas class="waves"></canvas></div>
    <button class="pause"></button>
  </div>
<?php endif; ?>