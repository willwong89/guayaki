<div class="film-grid" >
  <?php if(have_rows('films' )) : ?> 
    <?php while( have_rows('films')) : the_row();   ?>
      <?php $image = get_sub_field('image'); ?>
      <a class="single-film-item swipebox-video" 
        rel="vimeo" href="<?php the_sub_field('video_url'); ?>" 
        style='background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url("<?php echo $image['sizes']['large']?>")'
      >
        <img src="" />
        <span><?php the_sub_field('title'); ?></span>
      </a>
  <?php endwhile; endif; ?>
</div>  