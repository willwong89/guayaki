 <form class="single-line-form"> 
    <input type="email" placeholder="Enter Email" />
    <input class="big-button" type="submit" value="submit" />
  </form>
  <div class="social-links">
    <a target="_blank" href="https://www.instagram.com/guayaki/"><?php get_template_part('/assets/images/svg/instagram.svg'); ?></a>
    <a target="_blank" href="https://www.facebook.com/guayaki/"><?php get_template_part('/assets/images/svg/facebook.svg'); ?></a>
    <a target="_blank" class="youtube-icon" href="https://www.youtube.com/user/guayaki"><?php get_template_part('/assets/images/svg/youtube.svg'); ?></a>
    <a target="_blank" href="https://twitter.com/guayaki"><?php get_template_part('/assets/images/svg/twitter.svg'); ?></a>
    <a target="_blank" href="https://pinterest.com/guayaki"><?php get_template_part('/assets/images/svg/pinterest.svg'); ?></a>
</div>