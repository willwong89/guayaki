<header 
  id="page-header" 
  role="banner"
  style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?  the_post_thumbnail_url( 'banner' ); ?>')  no-repeat fixed center center/cover"
>
  <div class="content">
    <h1><?php the_title(); ?></h1>
  </div>
</header> 