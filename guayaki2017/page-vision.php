<?php
/*
Page: Our Vision
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<?php while ( have_posts() ) : the_post(); ?>

  <section  class="content-section -fat">
    <div class="row align-center">
      <div class="columns large-7 medium-9 small-12">
       <?php the_content(); ?>
    </div>
  </section>

  <section id="cebadores" style="background-image:url(<?php luc_img_dir();?>placeholder/farmers.jpg") class="content-section  -bg -fat">
    <div class="row">
      <div class="columns  large-7 medium-9 small-12">

      </div>
    </div>
  </section>

  <section  class="content-section mission -fat">
    <div class="row align-center">
      <div class="columns large-7 medium-9 small-12">
        <?php the_field('section_2') ?>
      </div>
    </div>
  </section>

  <section  class="content-section vision-films -slim row">
    <div class="columns large-12">
      <h2 class="icon-header">Watch</h2>
      <?php get_template_part( 'template-parts/film-grid' ); ?>
    </div>
  </section>

  <section  class="content-section mission -slim">

    <div class="row align-center text-center" style="padding-top:0;padding-bottom:30px;">
      <div class="columns large-7 medium-9 small-12">
        <?php the_field('section_3') ?>
      </div>
    </div>

    <div class="row align-center text-center">
      <div class="columns large-12 medium-12 small-12">
        <?php
          $button_1_text = get_field('button_1_text');
          $button_1_link = get_field('button_1_link');
          $button_2_text = get_field('button_2_text');
          $button_2_link = get_field('button_2_link');
        ?>
        <?php if( $button_1_text != '' && $button_1_link != '' ): ?>
          <a href="<?php echo $button_1_link; ?>" class="big-button" target="_blank"><?php echo $button_1_text; ?></a>
        <?php endif; ?>
        <?php if( $button_2_text != '' && $button_2_link != '' ): ?>
          <a href="<?php echo $button_2_link; ?>" class="big-button" target="_blank"><?php echo $button_2_text; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </section>

<?php endwhile; ?>
<?php get_footer();
