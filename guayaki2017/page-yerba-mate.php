<?php
/*
Page: Yerba Mate
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<section  class="content-section">
  <div class="row align-center">
    <div class="columns large-8 medium-10 small-12">
    <?php the_field('content_section_1'); ?>
  </div>
</section>

<section id="cebadores" class="content-section -bg">
  <div class="row">
    <div class="columns large-8 medium-10 small-12">
      <?php the_field('content_section_2'); ?>
    </div>
  </div>
</section>

<section  class="content-section section-3 ">
  <div class="row align-center">
    <div class="columns large-8 medium-10 small-12">
      <?php the_field('content_section_3'); ?>
    </div>
</section>

<?php if( have_rows('films') ): ?>
  <section class="content-section film-section">
    <div class="row">
      <div class="columns small-12">
        <h2 class="icon-header">Watch</h2>
        <?php get_template_part( 'template-parts/film-grid' ); ?>
    </div>
  </div>
</section>

<?php endif; ?>



 

<?php get_footer();
