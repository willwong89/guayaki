<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

</section>

	<footer id="footer">
    <div class="row align-center" id="footer-content">
  		<div id="footer-menu"  class="" role="navigation">
        <a href="/" class="wreath-home-link" ><?php get_template_part('/assets/images/svg/guay-wreath.svg'); ?></a>
  			<?php wp_nav_menu( array('menu_class' => 'footer-nav'	)); ?>
      </div>

      <div class="footer-connect columns small-12 medium-10 large-7">
        <?php get_template_part('template-parts/connect-group'); ?>
      </div>

  	</footer>
  </div>

<?php do_action( 'foundationpress_layout_end' ); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=“https://www.googletagmanager.com/gtag/js?id=UA-122242182-1“></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-122242182-1');
</script>
<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
