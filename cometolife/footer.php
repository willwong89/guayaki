<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

</section>

	<footer id="footer">
    <div class="row align-center" id="footer-content">
      <video class="bg-video" width="100%" height="100%" autoplay loop muted id="nav-video">
       <!--<source src="<?php echo get_stylesheet_directory_uri();?>/assets/video/blue-bg.mp4" type="video/mp4">-->
      </video>
  		<div id="footer-menu"  class="" role="navigation">
        <a href="/" class="wreath-home-link" ><?php get_template_part('/assets/images/svg/guay-wreath.svg'); ?></a>
  			<!--<p>Receive the latest stories.  We'll send you new films, exclusive articles and other info on a weekly basis.</p>-->
      </div>

      <div class="footer-connect columns small-12 medium-10 large-7">
        <?php get_template_part('template-parts/connect-group'); ?>
      </div>

  	</footer>
  </div>

<?php do_action( 'foundationpress_layout_end' ); ?>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
